@extends('layouts.main')

@section('content')
    

<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>TENTANG <span>SAYA</span></h1>
    <span class="title-bg">Tentang Saya</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
    <div class="row">
            <!-- Personal Info Starts -->
            <div class="col-12 col-lg-5 col-xl-6">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-uppercase custom-title mb-0 ft-wt-600">Biodata Diri</h3>
                    </div>
                    <div class="col-12 d-block d-sm-none">
                        <img src="img/krisna.jpg" class="img-fluid main-krisna" alt="my picture" />
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">First Name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Putu Sastra</span> </li>
                            <li> <span class="title">Last Name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Krisna Yana</span> </li>
                            <li> <span class="title">Age :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">20 Years</span> </li>
                            <li> <span class="title">Nationality :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span> </li>
                            <li> <span class="title">Freelance :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Mahasiswa</span> </li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Alamat :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Tabanan</span> </li>
                            <li> <span class="title">Phone :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">083851767867</span> </li>
                            <li> <span class="title">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">krisnayana91@gmail.com</span> </li>
                            <li> <span class="title">Instagram :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">sastra566</span> </li>
                            <li> <span class="title">facebook :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Putu Sastra Krisna Yana</span> </li>
                        </ul>
                    </div>
                    </div>
            </div>
            <!-- Personal Info Ends -->
            <!-- Boxes Starts -->
            <div class="col-12 col-lg-7 col-xl-6 mt-5 mt-lg-0">
                <div class="row">
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">Hobby</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Berkebun<span class="d-block">Sepak Bola</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">Musik</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Rege<span class="d-block">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">Film</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Action<span class="d-block">Animation</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">Food</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Mei Goreng<span class="d-block">Serombotan</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Boxes Ends -->
            <div class="col-12 mt-3">
                        <a href="about" class="btn btn-download ">Download CV</a>
             </div>
        </div>
        <hr class="separator">
        <!-- Skills Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">My Skills</h3>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p50">
                    <span>50%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">html</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p50">
                    <span>50%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">css</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p50">
                    <span>50%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">php</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p70">
                    <span>60%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Office</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p60">
                    <span>60%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Photohop</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p40">
                    <span>75%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Premiere Pro</h6>
            </div>
        </div>
        <!-- Skills Ends -->
        <hr class="separator mt-1">
        <!-- Experience & Education Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Riwayat Pendidikan <span>&</span> Pengalaman</h3>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2007 - 2013</span>
                            <h5 class="poppins-font text-uppercase">Sekolah Dasar <span class="place open-sans-font">SD Negeri 1 Tegal Mengkeb</span></h5>
                            <p class="open-sans-font">SD Negeri 1 Tegal Mengkeb merupakan SD yang terletak di Desa Tegal Mengkeb, Kecamatan Selemadeg Timur, Tabanan, Bali</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2013 - 2016</span>
                            <h5 class="poppins-font text-uppercase">Sekolah Menengah Pertama <span class="place open-sans-font">SMP Negeri 2 Selemadeg Timur</span></h5>
                            <p class="open-sans-font">SMP Negeri 2 Selemadeg Timur merupakan SMP yang terletak di Desa Tanguntiti, Kecamatan Selemag Timur, Tabanan, Bali</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2016 - 2019</span>
                            <h5 class="poppins-font text-uppercase">Sekolah Menengah Atas <span class="place open-sans-font">SMA Negeri 1 Selemadeg</span></h5>
                            <p class="open-sans-font">SMA Negeri 1 Selemadeg merupakan SMA yang terletak di Desa Bajra, Kecamatan Selemadeg, Tabanan, Bali</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2019 - Sekarang</span>
                            <h5 class="poppins-font text-uppercase">Perguruan Tinggi <span class="place open-sans-font">Undiksha</span></h5>
                            <p class="open-sans-font">Universitas Pendidikan Ganesha (Undiksha) merupakan salah satu perguruan tinggi negeri yang terletak di Kota Singaraja, Bali</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-trophy"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2020-2021</span>
                            <h5 class="poppins-font text-uppercase">Anggota<span class="place open-sans-font">Bidang 3</span></h5>
                            <p class="open-sans-font">BEM Fakultas Teknik dan Kejuruan, Universitas Pendidikan Ganesha</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-trophy"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2020</span>
                            <h5 class="poppins-font text-uppercase">Koordinator <span class="place open-sans-font">Official Basket</span></h5>
                            <p class="open-sans-font">Kegiatan DIES NATALIS, BEM Fakultas Teknik dan Kejuruan, Universitas Pendidikan Ganesha</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Experience & Education Ends -->
    </div>
</section>
<!-- Main Content Ends -->

@endsection